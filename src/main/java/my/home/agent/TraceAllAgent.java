package my.home.agent;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

/**
 * Trace all public method calls on all classes.
 * Let AspectJ modify the target methods so replacement methods are called instead.
 * Load Time Weaving" (LTW) is intended for this agent, so no recompilation.
 * 
 * @author Jan Uhlir
 */
@Aspect
public class TraceAllAgent {
	
	/**
	 * Define AOP point cut. Intercept all public methods on all classes,
	 * including library ones. Also static methods are covered. What is not
	 * covered are constructors.
	 * <p>
	 * Define only one essential exception, to avoid circular reference to
	 * itself, exclude whole 'my.home.agent' package
	 * <p>
	 * Which packages and classes will be intercepted is also determined in
	 * aop.xml. Affected classes are intersection of these two definitions.
	 */
	@Pointcut("execution(public * *(..)) && !within(my.home.agent..*)")
	public void allMethodsAllClasses() {}
	
	/**
	 * Intercept all constructors.  
	 */
	@Pointcut("execution(*.new(..)) && !within(my.home.agent..*)")
	public void allConstructorsAllClasses() {}
	
	/**
	 * Intercept one particular method only.
	 * For explanatory reasons.   
	 */
	@Pointcut("execution(public void my.home.sampleapplication.SampleApp.somePublicMethod(..)) && !within(my.home.agent..*)")
	public void adviceOneParticularMethodOnly() {}
	
	/**
	 * Define advice for a point cut. The interceptor implementation.
	 * 
	 * @param joinPoint
	 */
	@Before("allMethodsAllClasses()")
	public void beforeTracedMethods(JoinPoint joinPoint) {
		String className = joinPoint.getStaticPart().getSignature().getDeclaringTypeName();
		String methodName = joinPoint.getStaticPart().getSignature().getName();
		System.out.println(">>> called class: " + className + "  method: " + methodName + "(..)");
		// next, traced method is called automatically.
	}
	
	/**
	 * Define advice for a point cut. The interceptor implementation. When
	 * intercepting constructors it is better to call out tracing method after
	 * the instance was created.
	 * 
	 * @param joinPoint
	 */
	@After("allConstructorsAllClasses()")
	public void beforeTracedConstructors(JoinPoint joinPoint) {
		String className = joinPoint.getStaticPart().getSignature().getDeclaringTypeName();
		System.out.println(">>> called class: " + className + "  constructor");
		// next, traced constructor is called automatically.
	}
	
	/**
	 * Intercept one particular method only.
	 * For explanatory reasons.   
	 * 
	 * @param joinPoint
	 */
	@After("adviceOneParticularMethodOnly()")
	public void beforeOneParticularMethod(JoinPoint joinPoint) {
		String className = joinPoint.getStaticPart().getSignature().getDeclaringTypeName();
		String methodName = joinPoint.getStaticPart().getSignature().getName();
		System.out.println(">>> SPECIAL ADVICE called class: " + className + "  method: " + methodName + "(..)");
		// next, traced method is called automatically.
	}
}

// changes: 8.11., 9.11.2012, 12.11.2012