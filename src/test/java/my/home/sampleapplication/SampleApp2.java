package my.home.sampleapplication;


/**
 * Sample application to test weaving of nested classes, interfaces, abstract classes, extending and implementing classes  
 * inheritance, abstract methods
 */
public class SampleApp2 {
	
	public interface IFoo {
		public int methodA();
	}
	
	public static abstract class FooAbstract implements IFoo {
		public int methodA() {
			System.out.println("inside FooAbstract::A");
			return 0;
		}
		public String methodB() {
			System.out.println("inside FooAbstract::B");
			return null;
		}
		public abstract String methodC();
	}
	
	public static class FooBase extends FooAbstract {
		public String methodC() {
			System.out.println("inside FooBase::C");
			return null;
		}
	}
	
	public static interface IFooFoo {
		public String methodD(String in);
	}
	
	public static class MyFoo extends FooBase implements IFooFoo {
		public String methodD(String in) {
			System.out.println("inside FooBase::C");
			return in;
		}
		
		public String methodE(String in) {
			System.out.println("inside FooBase::C");
			return in;
		}
		
		public void runThemAll() {
			methodA();
			methodB();
			methodC();
			methodD("D");
			methodE("E");
		}
	}

	public static void main(String[] args) {
		MyFoo myFoo = new MyFoo();
		myFoo.runThemAll();
		System.out.println("Result of methodA=" + myFoo.methodA());
		System.out.println("Result of methodE=" + myFoo.methodE("E"));
	}
}
