package my.home.sampleapplication;


public class SampleApp {

	public SampleApp() {
		System.out.println("inside constructor");
	}
	
	public void somePublicMethod() {
		System.out.println("inside somePublicMethod");
		somePrivateMethod();
	}

	private void somePrivateMethod() {
		System.out.println("inside somePrivateMethod");
	}

	public static void main(String[] args) {
		SampleApp application = new SampleApp();
		application.somePublicMethod();
	}
}
