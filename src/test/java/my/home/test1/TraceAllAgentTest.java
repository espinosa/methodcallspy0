package my.home.test1;

import java.util.logging.Logger;

import junit.framework.Assert;
import my.home.sampleapplication.SampleApp;
import my.home.test.utils.Utils;

import org.junit.Test;


/**
 * Integration test for run time modification of sample application using load
 * time weaving provided by AspectJ Weaver.
 * 
 * @author Jan Uhlir
 */
public class TraceAllAgentTest {
	private final Logger logger = Logger.getLogger(TraceAllAgentTest.class.getName());

	/**
	 * To test load time weaving (LTW) we need to fork a new process, start new
	 * JVM with appropriate command line arguments, capturing he stream piped
	 * from the standard output stream of the forked process.
	 * <p>
	 * Note: we really have to use <code>p.getInputStream()</code>, it is not a
	 * mistake. The output from forked process is the input stream, stream to be
	 * read, from the perspective of this test process. Hence we need to get
	 * input stream from the tested application process.
	 * <p>
	 * Note: there should be NO output to stdout. The full output is logged
	 * using 'fine' level. If you want to see full sample application output,
	 * set 'fine' logging level for this test class. There may be some logging
	 * messages from AspectJ weaving process of interest.
	 */
	@Test
	public void testedInterceptedApplicationShouldReturnExpectedLogsInStandardOutput() throws Exception {
		String classpath = System.getProperty("java.class.path");
		String aopConfPath = Utils.getClassFullDirectory(this.getClass());
		String aspectjWeaverJarFullPath = Utils.getAspectjWeaverJarFullPath();
		ProcessBuilder pb = new ProcessBuilder(
				"java", 
				"-cp", aopConfPath + classpath,  // they MUST BE separated as two arguments, otherwise you get -cp is not recognised option - 
				"-javaagent:" + aspectjWeaverJarFullPath,
				SampleApp.class.getName()
				);
		pb.redirectErrorStream(true);
		Process p = pb.start();
		String capturedOutput = Utils.dumpProcessOutputToString(p.getInputStream());
		int exitValue = p.waitFor();
		logger.fine(capturedOutput);
		Assert.assertEquals(0, exitValue);
		
		Assert.assertEquals("",
				Utils.filterLinesStartingWith("\\[(.*)\\] error", capturedOutput));  // that is how AspectJ reports weaving errors
		
		Assert.assertEquals("" +
				">>> called class: my.home.sampleapplication.SampleApp  method: main(..)\n" +
				">>> called class: my.home.sampleapplication.SampleApp  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleApp  method: somePublicMethod(..)\n" +
				">>> SPECIAL ADVICE called class: my.home.sampleapplication.SampleApp  method: somePublicMethod(..)",
				Utils.filterLinesStartingWith(">>>", capturedOutput));
	}
}
