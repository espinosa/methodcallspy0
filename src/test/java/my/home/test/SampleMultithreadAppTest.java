package my.home.test;

import java.util.logging.Logger;

import junit.framework.Assert;
import my.home.sampleapplication.SampleMultithreadedApp;
import my.home.test.utils.Utils;

import org.junit.Test;


/**
 * Integration test for run time modification of sample application using load
 * time weaving provided by AspectJ Weaver.
 * 
 * @author Jan Uhlir
 */
public class SampleMultithreadAppTest {
	private final Logger logger = Logger.getLogger(SampleMultithreadAppTest.class.getName());

	/**
	 * Trace multithreaded application 
	 * <p>
	 * For more details see {@link SampleApp1Test#testedInterceptedApplicationShouldReturnExpectedLogsInStandardOutput()}. 
	 */
	@Test
	public void targetApplicationShouldReturnExpectedLogsInStandardOutputForMultiThreadedApplication() throws Exception {
		String classpath = System.getProperty("java.class.path");
		String aspectjWeaverJarFullPath = Utils.getAspectjWeaverJarFullPath();
		ProcessBuilder pb = new ProcessBuilder(
				"java", 
				"-cp", classpath,  // they MUST BE separated as two arguments, otherwise you get -cp is not recognised option - 
				"-javaagent:"+aspectjWeaverJarFullPath,
				SampleMultithreadedApp.class.getName()
				);
		pb.redirectErrorStream(true);
		Process p = pb.start();
		String result = Utils.dumpProcessOutputToString(p.getInputStream());
		int exitValue = p.waitFor();
		Assert.assertEquals(0, exitValue);
		
		Assert.assertEquals("",
				Utils.filterLinesStartingWith("\\[(.*)\\] error", result));  // that is how AspectJ reports weaving errors
		
		Assert.assertEquals("" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp  method: main(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodA(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodA(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodA(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodA(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodA(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodA(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodB(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodB(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodB(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodB(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodB(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodB(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodB(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodC(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodC(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodC(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodC(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodC(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodC(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodC(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodC(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$AbstractWorker  method: methodC(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$UnusedClass  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$Worker1  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$Worker1  method: run(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$Worker2  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$Worker2  method: run(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$Worker3  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$Worker3  method: run(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$WorkerA  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$WorkerA  method: run(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$WorkerB  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$WorkerB  method: run(..)\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$WorkerC  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleMultithreadedApp$WorkerC  method: run(..)\n",
				Utils.sortMultilineText(Utils.filterLinesStartingWith(">>>", result))
				);
		
		logger.fine(result);
	}
}
