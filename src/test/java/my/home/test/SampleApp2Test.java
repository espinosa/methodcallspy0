package my.home.test;

import java.util.logging.Logger;

import junit.framework.Assert;
import my.home.sampleapplication.SampleApp2;
import my.home.test.utils.Utils;

import org.junit.Test;


/**
 * Integration test for run time modification of sample application using load
 * time weaving provided by AspectJ Weaver.
 * 
 * @author Jan Uhlir
 */
public class SampleApp2Test {
	private final Logger logger = Logger.getLogger(SampleApp2Test.class.getName());

	/**
	 * Trace more complex application with nested classes. 
	 * <p>
	 * For more details see {@link SampleApp1Test#testedInterceptedApplicationShouldReturnExpectedLogsInStandardOutput()}. 
	 */
	@Test
	public void targetApplicationShouldReturnExpectedLogsInStandardOutputForSampleAppWithNestedClasses() throws Exception {
		String classpath = System.getProperty("java.class.path");
		String aspectjWeaverJarFullPath = Utils.getAspectjWeaverJarFullPath();
		ProcessBuilder pb = new ProcessBuilder(
				"java", 
				"-cp", classpath, 
				"-javaagent:"+aspectjWeaverJarFullPath,
				SampleApp2.class.getName()
				);
		pb.redirectErrorStream(true);
		Process p = pb.start();
		String result = Utils.dumpProcessOutputToString(p.getInputStream());
		int exitValue = p.waitFor();
		Assert.assertEquals(0, exitValue);
		
		Assert.assertEquals("",
				Utils.filterLinesStartingWith("\\[(.*)\\] error", result));  // that is how AspectJ reports weaving errors
		
		Assert.assertEquals("" +
				">>> called class: my.home.sampleapplication.SampleApp2  method: main(..)\n" +
				">>> called class: my.home.sampleapplication.SampleApp2$FooAbstract  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleApp2$FooBase  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleApp2$MyFoo  constructor\n" +
				">>> called class: my.home.sampleapplication.SampleApp2$MyFoo  method: runThemAll(..)\n" +
				">>> called class: my.home.sampleapplication.SampleApp2$FooAbstract  method: methodA(..)\n" +
				">>> called class: my.home.sampleapplication.SampleApp2$FooAbstract  method: methodB(..)\n" +
				">>> called class: my.home.sampleapplication.SampleApp2$FooBase  method: methodC(..)\n" +
				">>> called class: my.home.sampleapplication.SampleApp2$MyFoo  method: methodD(..)\n" +
				">>> called class: my.home.sampleapplication.SampleApp2$MyFoo  method: methodE(..)\n" +
				">>> called class: my.home.sampleapplication.SampleApp2$FooAbstract  method: methodA(..)\n" +
				">>> called class: my.home.sampleapplication.SampleApp2$MyFoo  method: methodE(..)",
				Utils.filterLinesStartingWith(">>>", result));
		
		logger.fine(result);
	}
}
