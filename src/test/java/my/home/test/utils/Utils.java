package my.home.test.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.Arrays;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import my.home.test.SampleMultithreadAppTest;

import org.aspectj.weaver.ltw.LTWeaver;

/**
 * Test utility classes
 * @author Jan Uhlir
 */
public class Utils {
	//public static final String ASPECTJ_WEAVER_ENTRY_CLASS = "org/aspectj/weaver/ltw/LTWeaver.class";
	public static final String ASPECTJ_WEAVER_ENTRY_CLASS = LTWeaver.class.getName().replace(".", "/").concat(".class");
	
	/**
	 * Try to find aspectj jar assuming it is in the classpath and current class
	 * loader is aware of aspectj classes. Run under Eclipse or Maven should
	 * honour such assumption. This implementation should deal well with
	 * pitfalls like multiple aspectj JARs in the classpath or unusual JAR
	 * name.
	 * 
	 * @param classpath
	 * @return full path of aspectj jar
	 */
	public static String getAspectjWeaverJarFullPath() {
		ClassLoader loader = SampleMultithreadAppTest.class.getClassLoader();
		String aspectjTypicalClass = ASPECTJ_WEAVER_ENTRY_CLASS;
		URL aspectjTypicalClassURL = loader.getResource(aspectjTypicalClass);
		if (aspectjTypicalClassURL==null) throw new RuntimeException("Class " + ASPECTJ_WEAVER_ENTRY_CLASS 
				+ " was not found on classpath. Is AspectJ jar on classpath?"); 
		String aspectjUrl =	aspectjTypicalClassURL.toExternalForm();
		return aspectjUrl.substring(aspectjUrl.indexOf('/') + 1, aspectjUrl.indexOf(".jar!") + 4);
	}

	/**
	 * Return directory where class file is located for given Class.
	 * @param clazz
	 * @return
	 */
	public static String getClassFullDirectory(Class<?> clazz) {
		ClassLoader loader = clazz.getClassLoader();
		String classFileUrl = loader.getResource(clazz.getName().replace(".", "/") + ".class").toExternalForm();
		return classFileUrl.substring(classFileUrl.indexOf('/') + 1, classFileUrl.lastIndexOf("/"));
	}


	/**
	 * Pipe given input stream to standard output stream. Useful to dump
	 * output from a forked process.
	 * <p>
	 * This method blocks current thread waiting from output of external process.
	 */
	public static void dumpProcessOutputToStdout(InputStream is) throws IOException {
		BufferedReader reader = new BufferedReader (new InputStreamReader(is));
		String line;
		while ((line = reader.readLine()) != null) {
			System.out.println(line);
		}
	}

	/**
	 * Pipe given input stream to standard output stream. Useful to dump
	 * output from a forked process.
	 * <p>
	 * This method blocks current thread waiting from output of external process.
	 */
	public static void dumpProcessOutputToLogger(InputStream is, Logger logger) throws IOException {
		BufferedReader reader = new BufferedReader (new InputStreamReader(is));
		String line;
		while ((line = reader.readLine()) != null) {
			logger.fine(line);
		}
	}


	/**
	 * Reprint given input stream to standard output stream. Useful to dump
	 * output from a forked process.
	 */
	public static String dumpProcessOutputToString(InputStream is) throws IOException {
		StringWriter sw = new StringWriter();
		PrintWriter out = new PrintWriter(sw);
		BufferedReader reader = new BufferedReader (new InputStreamReader(is));
		String line;
		while ((line = reader.readLine()) != null) {
			out.println(line);
		}
		return sw.toString();
	}

	/**
	 * Return only lines with given prefix from given text.
	 * @param startWith filtering string, must be compatible with regular expressions, some characters may require 'escaping'
	 * @param inputText expected multi line text, text to filter
	 * @return only lines starting with given prefix
	 */
	public static String filterLinesStartingWith(String startWith, String inputText) {
		StringBuilder sb = new StringBuilder();
		Pattern p = Pattern.compile("^" + startWith);
		BufferedReader sr = new BufferedReader(new StringReader(inputText));
		String line;
		try {
			while ((line = sr.readLine()) != null) {
				Matcher m = p.matcher(line);
				if (m.find()) {
					if (sb.length()>0) {
						sb.append("\n");
					}
					sb.append(line);
				}
			}
		} catch (IOException e) {
			// should never get here, it is a StringReader! memory only operations
			throw new RuntimeException(e);
		}
		return sb.toString();
	}

	/**
	 * Sort multi line text
	 * @param s input text
	 * @return multi line text with potentially changed order of lines
	 */
	public static String sortMultilineText(String s) {
		String[] lines = s.split("\n");
		Arrays.sort(lines);
		StringBuilder sb = new StringBuilder(lines.length*2);
		for (String line : lines) {
			sb.append(line);
			sb.append("\n");
		}
		return sb.toString();
	}
}
